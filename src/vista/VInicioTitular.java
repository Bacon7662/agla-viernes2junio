package vista;

import javax.swing.*;
import java.awt.Color;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.BevelBorder;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import controlador.CImplementacionControl;
import controlador.Controlador;
import modelo.MImplementacion;
import modelo.Modelo;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class VInicioTitular extends JFrame implements Vista {

	private CImplementacionControl controlador;
	private MImplementacion modelo;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textFieldFAX;
	private JTextField textFieldCorreo;
	private JTextField textFieldTLFIJO;
	private JTextField textFieldMovil;
	private JTextField textFieldMunicipio;
	private JTextField textFieldNombre;
	private JTextField textRSocial;
	private JTextField textFieldDireccion;
	private JTextField textApellidos;
	private JTextField textDNI;
	private JTextField textFieldCP;
	private JComboBox comboBox;

	public VInicioTitular() {
		setBounds(100, 100, 950, 592);
		//setSize(970,700);
		setLocationRelativeTo(null);
		
		
		setTitle("REGISTRO DE PERSONAS FISICA / JURIDICA");
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(VInicioTitular.class.getResource("/GestionLicencias/IMG/386px-Escudo-torrijos.svg.png")));
		//setBounds(100, 100, 950, 592);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(95, 158, 160));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(panel,
				GroupLayout.DEFAULT_SIZE, 934, Short.MAX_VALUE));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(panel,
				GroupLayout.DEFAULT_SIZE, 554, Short.MAX_VALUE));

		JButton button = new JButton("SIGUIENTE");
		button.addActionListener(new ActionListener() {
			
			
			
			
			public void actionPerformed(ActionEvent e) {
				String sDNI = textDNI.getText();
				
				int res=controlador.insertarATitular(sDNI, textFieldNombre.getText(), textApellidos.getText(),
						textFieldMunicipio.getText(), Integer.parseInt(textFieldCP.getText()),
						textFieldDireccion.getText(),Integer.parseInt( textFieldMovil.getText()),Integer.parseInt(textFieldTLFIJO.getText()),
						textFieldCorreo.getText(), textFieldFAX.getText(), comboBox.getSelectedItem().toString(),
						textRSocial.getText());
				if(res!=-1){
					
					controlador.insertarALicenciaTipo2(sDNI);
					JOptionPane.showMessageDialog(null, "Petici�n procesada");
					dispose();
				}
				
				
			}
		});
		
		
		
		button.setIcon(new ImageIcon(VInicioTitular.class.getResource("/GestionLicencias/IMG/next.png")));

		JButton button_1 = new JButton("CANCELAR");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		button_1.setIcon(new ImageIcon(VInicioTitular.class.getResource("/GestionLicencias/IMG/cancel.png")));

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(220, 220, 220));
		panel_1.setBorder(new TitledBorder(null, "INTRODUCE LOS DATOS DEL SOLICITANTE", TitledBorder.CENTER,
				TitledBorder.TOP, null, new Color(59, 59, 59)));

		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "FISICA", "JURIDICA" }));

		JLabel label = new JLabel("JURIDICA / FISICA");

		textFieldFAX = new JTextField();
		textFieldFAX.setColumns(10);

		JLabel label_1 = new JLabel("FAX");

		JLabel label_2 = new JLabel("CORREO ELECTR\u00D3NICO");

		textFieldCorreo = new JTextField();
		textFieldCorreo.setColumns(10);

		textFieldTLFIJO = new JTextField();
		textFieldTLFIJO.setColumns(10);

		JLabel label_3 = new JLabel("TELF FIJO");

		JLabel label_4 = new JLabel("TELF M\u00D3VIL");

		textFieldMovil = new JTextField();
		textFieldMovil.setColumns(10);

		textFieldMunicipio = new JTextField();
		textFieldMunicipio.setColumns(10);

		JLabel label_5 = new JLabel("MUNICIPIO*");

		JLabel label_6 = new JLabel("NOMBRE*");

		JLabel label_7 = new JLabel("RAZ\u00D3N SOCIAL*");

		textFieldNombre = new JTextField();
		textFieldNombre.setColumns(10);

		textRSocial = new JTextField();
		textRSocial.setColumns(10);

		textFieldDireccion = new JTextField();
		textFieldDireccion.setColumns(10);

		JLabel label_8 = new JLabel("DIRECCI\u00D3N*");

		textApellidos = new JTextField();
		textApellidos.setColumns(10);

		JLabel label_9 = new JLabel("APELLIDOS*");

		JLabel label_10 = new JLabel("DNI/NIE/CIF*");

		textDNI = new JTextField();
		textDNI.setColumns(10);

		JLabel label_11 = new JLabel("C\u00D3DIGO POSTAL");

		textFieldCP = new JTextField();
		textFieldCP.setColumns(10);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addComponent(label_7, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(6)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
										.addComponent(label_6, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
										.addComponent(label_8, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
										.addComponent(textFieldDireccion, GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
										.addGroup(gl_panel_1.createSequentialGroup()
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(textRSocial, GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE))
										.addComponent(textFieldNombre, GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE))
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
										.addGroup(gl_panel_1.createSequentialGroup()
											.addGap(20)
											.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
												.addComponent(textApellidos, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE)
												.addComponent(label_9, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
												.addComponent(label_5, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)))
										.addGroup(gl_panel_1.createSequentialGroup()
											.addGap(18)
											.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
												.addComponent(textFieldMunicipio)
												.addGroup(gl_panel_1.createSequentialGroup()
													.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
														.addComponent(textDNI, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE)
														.addComponent(label_10, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
														.addComponent(textFieldFAX, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
														.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE))
													.addGap(29)
													.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
														.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
														.addComponent(textFieldTLFIJO, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)))))))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
										.addComponent(textFieldCorreo, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
										.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE))))
							.addGap(12)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
									.addComponent(textFieldCP)
									.addGroup(gl_panel_1.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(textFieldMovil, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE)))
								.addComponent(label, GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
								.addComponent(label_11, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING, false)
									.addComponent(comboBox, Alignment.LEADING, 0, 121, Short.MAX_VALUE)
									.addComponent(label_4, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)))))
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(32)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_9, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_6, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(label))
					.addGap(18)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(textFieldNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textApellidos, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_8, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_5, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_11, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(textFieldDireccion, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldMunicipio, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldCP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_7, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_10, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(textRSocial, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
						.addComponent(textDNI, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldTLFIJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldMovil, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(textFieldCorreo, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldFAX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(139))
		);
		panel_1.setLayout(gl_panel_1);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 922, Short.MAX_VALUE)
						.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
							.addComponent(button_1, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 646, Short.MAX_VALUE)
							.addComponent(button, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(82)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 343, GroupLayout.PREFERRED_SIZE)
					.addGap(63)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(button_1, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
						.addComponent(button, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(26, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		getContentPane().setLayout(groupLayout);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}

		VInicioTitular ventana = new VInicioTitular();

	}

	@Override
	public void setModelo(Modelo modelo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setControlador(CImplementacionControl controlador) {
		// TODO Auto-generated method stub
		this.controlador=controlador;
	}

}