package modelo;

import controlador.Controlador;
import vista.VFichero;
import vista.VPantallaPrincipal;
import vista.Vista;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.*;
import java.util.Properties;

import javax.naming.spi.DirStateFactory.Result;

public class MImplementacion implements Modelo {

	private VPantallaPrincipal vista;
	private VFichero vista2;

	public VFichero getVista2() {
		return vista2;
	}

	private Connection conexion;
	private String Usuario;
	private String Pass;
	private String bbdd;

	// Datos
	/*
	 * String descripcion; String actividad; String catastro; String
	 * fechaInicio; String solicitud; String fotocopiaDNI; String
	 * fotocopiaImpuestos; String fotografia; String fotocopiaObra; String
	 * fotocopiaEscritura; String fotocopiaMemoria; String fotocopiaPago; String
	 * fotocopiaOtras; String certificado1; String certificado2; String planos;
	 * 
	 * String razonSocial; String cif; String fax; String tipoPersona; String
	 * tlfMovil; String tlfFijo; String direccion; String municipio; String cp;
	 * String email;
	 */
	String solicitud;

	// Titular
	String sCIF_NIF_NIE;
	String sREGISTRO;
	String sNOMBRE;
	String sAPELLIDOS;
	String sMUNICIPIO;
	int iCOD_POSTAL;
	String sDIRECCION;
	int iTELEFONO_MOVIL;
	int iTELEFONO_FIJO;
	String sEMAIL;
	String sFAX;
	String sTIPO_SOLICITANTE;
	String sFOTOCOPIA_DNI_CIF_NIE;
	String sFOTOGRAFIAS;
	String sFOTOCOPIA_ACTIVIDAD;
	String sFOTOCOPIA_ESCRITURAS;
	String sJUSTIFICANTE_PAGO;
	String sFOTOCOPIA_OTROS;
	String sRAZON_SOCIAL;

	// licencia
	String sDescripcionActividad;
	String ACTIVIDAD;
	String sREF_CATASTRAL;
	Date dFechaInicioActividad;
	Date dFechaSolicitud;
	String sFOTOCOPIA_OBRA;
	String sMEMORIA_ACTIVIDAD;
	String sCERTIFICADO_TECNICO1;
	String sCERTIFICADO_COLEGIO_OFICIAL2;
	String sPLANOS_LOCAL;
	String sTIPO_SUELO;
	String sEMPLAZAMIENTO;
	String sDNI_titular;
	String sEstadoLicencia;

	public void setBBDD(String Usuario, String Pass, String bbdd) {
		this.Usuario = Usuario;
		this.Pass = Pass;
		this.bbdd = bbdd;

		Properties propiedades = new Properties();
		OutputStream salida = null;
		try {
			File miFichero = new File("configuracion.ini");
			if (miFichero.exists()) {
				salida = new FileOutputStream(miFichero);

				propiedades.setProperty("Usuario", this.Usuario);
				propiedades.setProperty("Pass", this.Pass);
				propiedades.setProperty("Url", this.bbdd);

				propiedades.store(salida, "Comentario para el fichero");
			} else
				System.err.println("Fichero no encontrado");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (salida != null) {
				try {
					salida.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void getBBDD() {
		Properties propiedades = new Properties();
		InputStream entrada = null;
		try {
			File miFichero = new File("configuracion.ini");
			if (miFichero.exists()) {
				entrada = new FileInputStream(miFichero);
				// cargamos el archivo de propiedades
				propiedades.load(entrada);
				// obtenemos las propiedades y las imprimimos
				this.Usuario = propiedades.getProperty("Usuario");
				this.Pass = propiedades.getProperty("Pass");
				this.bbdd = propiedades.getProperty("Url");
			} else
				System.err.println("Fichero no encontrado");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (entrada != null) {
				try {
					entrada.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (vista2 != null) {
			vista2.actualizarDatosBBDD();
		}

	}

	public String getUsuario() {
		return Usuario;
	}

	public void setUsuario(String usuario) {
		Usuario = usuario;
	}

	public Connection getConexion() {
		return conexion;
	}

	public void setConexion(Connection conexion) {
		this.conexion = conexion;
	}

	public String getPass() {
		return Pass;
	}

	public void setPass(String pass) {
		Pass = pass;
	}

	public String getBbdd() {
		return bbdd;
	}

	public void setBbdd(String bbdd) {
		this.bbdd = bbdd;
	}

	public String ConexionBBDD() {
		String aviso;
		try {
			getBBDD();
			Class.forName("com.mysql.jdbc.Driver");
			conexion = DriverManager.getConnection(this.bbdd, this.Usuario, this.Pass);
			System.out.println(" - Conexi�n con MySQL establecida -");
			aviso = "�Conexi�n satisfactoria!";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(" � Error de Conexi�n con MySQL -");
			aviso = "�Conexi�n Fallida!";
			e.printStackTrace();
		}
		return aviso;

	}

	
	//////////////////////////////// INSERTS EN BASE DE DATOS//////////////////////////

	public int insertarARegistra(String sDNI_funcionario, String sCatastro_Licencia) {
		int idNuevo = -1;
		try {
			if (conexion == null) {
				System.out.println("NO EXISTE");
				System.exit(-1);
			}
			String query = "INSERT INTO registra (DNI_funcionario, catastro_Licencia) VALUES ('" + sDNI_funcionario
					+ "','" + sCatastro_Licencia + "')";
			System.out.println("MI QUERY ES: " + query);

			PreparedStatement pstmt = conexion.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			pstmt.executeUpdate();

			ResultSet tableKeys = pstmt.getGeneratedKeys();
			tableKeys.next();
			idNuevo = tableKeys.getInt(1);

			System.out.println("ID NUEVO : " + idNuevo);

			if (pstmt != null)
				pstmt.close();

		} catch (SQLException s) {
			s.printStackTrace();
		}

		return idNuevo;
	}

	public int insertarATitular(String sCIF_NIF_NIE, String sREGISTRO, String sNOMBRE, String sAPELLIDOS,
			String sMUNICIPIO, int iCOD_POSTAL, String sDIRECCION, int iTELEFONO_MOVIL, int iTELEFONO_FIJO,
			String sEMAIL, String sFAX, String sTIPO_SOLICITANTE, String sFOTOCOPIA_DNI_CIF_NIE, String sFOTOGRAFIAS,
			String sFOTOCOPIA_ACTIVIDAD, String sFOTOCOPIA_ESCRITURAS, String sJUSTIFICANTE_PAGO,
			String sFOTOCOPIA_OTROS, String sRAZON_SOCIAL) {
		int idNuevo = -1;
		try {
			if (conexion == null) {
				System.out.println("NO EXISTE");
				System.exit(-1);
			}
			String query = "INSERT INTO titular (CIF_NIF_NIE, REGISTRO, NOMBRE, APELLIDOS, MUNICIPIO, COD_POSTAL, DIRECCION, TELEFONO_MOVIL, TELEFONO_FIJO, EMAIL, FAX, TIPO_SOLICITANTE, FOTOCOPIA_DNI_CIF_NIE, FOTOGRAFIAS, FOTOCOPIA_ACTIVIDAD, FOTOCOPIA_ESCRITURAS, JUSTIFICANTE_PAGO, FOTOCOPIA_OTROS, RAZON_SOCIAL) "
					+ "VALUES ('" + sCIF_NIF_NIE + "','" + sREGISTRO + "','" + sNOMBRE + "','" + sAPELLIDOS + "','"
					+ sMUNICIPIO + "','" + iCOD_POSTAL + "','" + sDIRECCION + "','" + iTELEFONO_MOVIL + "','"
					+ iTELEFONO_FIJO + "','" + sEMAIL + "','" + sFAX + "','" + sTIPO_SOLICITANTE + "','"
					+ sFOTOCOPIA_DNI_CIF_NIE + "','" + sFOTOGRAFIAS + "','" + sFOTOCOPIA_ACTIVIDAD + "','"
					+ sFOTOCOPIA_ESCRITURAS + "','" + sJUSTIFICANTE_PAGO + "','" + sFOTOCOPIA_OTROS + "','"
					+ sRAZON_SOCIAL + "')";

			System.out.println("MI QUERY ES: " + query);

			PreparedStatement pstmt = conexion.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			pstmt.executeUpdate();

			ResultSet tableKeys = pstmt.getGeneratedKeys();
			tableKeys.next();
			idNuevo = tableKeys.getInt(1);

			System.out.println("ID NUEVO : " + idNuevo);

			if (pstmt != null)
				pstmt.close();

		} catch (SQLException s) {
			s.printStackTrace();
		}

		return idNuevo;
	}

	public int insertarALicencia(String sREF_CATASTRAL, String sTIPO_SUELO, String sEMPLAZAMIENTO, String ACTIVIDAD,
			String sMEMORIA_ACTIVIDAD, String sPLANOS_LOCAL, String sFOTOCOPIA_OBRA, String sCERTIFICADO_TECNICO1,
			String sCERTIFICADO_COLEGIO_OFICIAL2, String sDNI_titular, String dFechaSolicitud, String dFechaInicioActividad,
			String sEstadoLicencia, String sDescripcionActividad) {
		int idNuevo = 1;
		try {
			
			if (conexion == null) {
				System.out.println("NO EXISTE");
				System.exit(-1);
			}
			String query = "INSERT INTO licencia (REF_CATASTRAL, TIPO_SUELO, EMPLAZAMIENTO, ACTIVIDAD, MEMORIA_ACTIVIDAD, PLANOS_LOCAL, FOTOCOPIA_OBRA, CERTIFICADO_TECNICO1, CERTIFICADO_COLEGIO_OFICIAL2, DNI_titular, FechaSolicitud, FechaInicioActividad, EstadoLicencia, DescripcionActividad) "
					+ "VALUES ('" + sREF_CATASTRAL + "','" + sTIPO_SUELO + "','" + sEMPLAZAMIENTO + "','" + ACTIVIDAD
					+ "','" + sMEMORIA_ACTIVIDAD + "','" + sPLANOS_LOCAL + "','" + sFOTOCOPIA_OBRA + "','"
					+ sCERTIFICADO_TECNICO1 + "','" + sCERTIFICADO_COLEGIO_OFICIAL2 + "','" + sDNI_titular + "','"
					+ dFechaSolicitud + "','" + dFechaInicioActividad + "','" + sEstadoLicencia + "','"
					+ sDescripcionActividad + "')";

			System.out.println("MI QUERY ES: " + query);

			PreparedStatement pstmt = conexion.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			pstmt.executeUpdate();

			ResultSet tableKeys = pstmt.getGeneratedKeys();
			/*tableKeys.next();
			idNuevo = tableKeys.getInt(1);*/

			System.out.println("ID NUEVO : " + idNuevo);

			if (pstmt != null)
				pstmt.close();

		} catch (SQLException s) {
			s.printStackTrace();
		}

		return idNuevo;
	}

	//////////////////////////////////RECOGER DATOS///////////////////////////////////////////////////
	public String[][] recogerDatosBBDDPersonas() {
		String resultados[][] = null;
		try {
			if (conexion == null) {
				System.out.println("NO EXISTE");
				System.exit(-1);
			}
			PreparedStatement pstmt = conexion.prepareStatement(
					"SELECT titular.CIF_NIF_NIE, titular.registro, titular.nombre, titular.apellidos, licencia.actividad FROM titular JOIN licencia ON licencia.DNI_titular=titular.CIF_NIF_NIE");
			ResultSet rset = pstmt.executeQuery();

			ResultSetMetaData rsmd = rset.getMetaData();
			int z = 0;
			while (rset.next()) {

				System.out.println(rset.getString("CIF_NIF_NIE") + " " + rset.getString("nombre") + " "
						+ rset.getString("apellidos") + " " + rset.getString("actividad") + " "
						+ rset.getString("registro"));
				z += 1;
			}

			rset.first();
			rset = pstmt.executeQuery();
			int i = 0;
			resultados = new String[z][rsmd.getColumnCount()];
			while (rset.next()) {
				String fila[] = new String[rsmd.getColumnCount()];
				fila[0] = rset.getString("CIF_NIF_NIE");
				fila[1] = rset.getString("nombre");
				fila[2] = rset.getString("apellidos");
				fila[3] = rset.getString("actividad");
				fila[4] = rset.getString("registro");

				resultados[i] = fila;
				System.out.println(rset.getString("CIF_NIF_NIE") + " " + rset.getString("nombre") + " "
						+ rset.getString("apellidos") + " " + rset.getString("actividad") + " "
						+ rset.getString("registro"));
				i++;
			}

		} catch (SQLException s) {
			s.printStackTrace();
		}
		return resultados;
	}

	public String[][] recogerDatosBBDDLicencias() {
		String resultados[][] = null;
		try {
			if (conexion == null) {
				System.out.println("NO EXISTE");
				System.exit(-1);
			}
			PreparedStatement pstmt = conexion.prepareStatement(
					"SELECT titular.registro, licencia.actividad, licencia.emplazamiento,licencia.tipo_suelo, licencia.EstadoLicencia FROM titular JOIN licencia ON licencia.DNI_titular=titular.CIF_NIF_NIE");
			ResultSet rset = pstmt.executeQuery();

			ResultSetMetaData rsmd = rset.getMetaData();
			int z = 0;
			while (rset.next()) {

				z += 1;
			}

			rset.first();
			rset = pstmt.executeQuery();
			int i = 0;
			resultados = new String[z][rsmd.getColumnCount()];
			while (rset.next()) {
				String fila[] = new String[rsmd.getColumnCount()];
				fila[0] = rset.getString("registro");
				fila[1] = rset.getString("actividad");
				fila[2] = rset.getString("emplazamiento");
				fila[3] = rset.getString("tipo_suelo");
				fila[4] = rset.getString("EstadoLicencia");
				resultados[i] = fila;

				i++;
			}

		} catch (SQLException s) {
			s.printStackTrace();
		}
		return resultados;
	}

	public void setVista(Vista vista) {
		this.vista = (VPantallaPrincipal) vista;

	}

	public void setVista2(Vista ven) {
		this.vista2 = (VFichero) ven;

	}

	public Vista getVista() {
		return vista;
	}

	public void recogerDatosExtra(String dato) {

		try {
			if (conexion == null) {
				System.out.println("NO EXISTE");
				System.exit(-1);
			}
			PreparedStatement pstmt = conexion.prepareStatement(
					"SELECT * FROM titular JOIN licencia ON licencia.DNI_titular=titular.CIF_NIF_NIE WHERE titular.REGISTRO= "
							+ dato);
			ResultSet rset = pstmt.executeQuery();

			// ResultSetMetaData rsmd = rset.getMetaData();
			int z = 0;
			while (rset.next()) {

				sDescripcionActividad = rset.getString("DescripcionActividad");
				ACTIVIDAD = rset.getString("ACTIVIDAD");
				sREF_CATASTRAL = rset.getString("REF_CATASTRAL");
				dFechaInicioActividad = rset.getDate("FechaInicioActividad");
				dFechaSolicitud = rset.getDate("FechaSolicitud");
				sFOTOCOPIA_DNI_CIF_NIE = rset.getString("Fotocopia_DNI_CIF_NIE");
				sFOTOCOPIA_ACTIVIDAD = rset.getString("FOTOCOPIA_ACTIVIDAD");
				sFOTOGRAFIAS = rset.getString("FOTOGRAFIAS");
				sFOTOCOPIA_OBRA = rset.getString("FOTOCOPIA_OBRA");
				sFOTOCOPIA_ESCRITURAS = rset.getString("FOTOCOPIA_ESCRITURAS");
				sMEMORIA_ACTIVIDAD = rset.getString("MEMORIA_ACTIVIDAD");
				sJUSTIFICANTE_PAGO = rset.getString("JUSTIFICANTE_PAGO");
				sFOTOCOPIA_OTROS = rset.getString("FOTOCOPIA_OTROS");
				sCERTIFICADO_TECNICO1 = rset.getString("CERTIFICADO_TECNICO1");
				sCERTIFICADO_COLEGIO_OFICIAL2 = rset.getString("CERTIFICADO_COLEGIO_OFICIAL2");
				sPLANOS_LOCAL = rset.getString("PLANOS_LOCAL");
				z += 1;
			}

		} catch (SQLException s) {
			s.printStackTrace();
		}

		vista.devolverDatosExtra();
	}

	/**
	 * 
	 * @param dato
	 */
	public void recogerDatosExtra2(String dato) {

		try {
			if (conexion == null) {
				System.out.println("NO EXISTE");
				System.exit(-1);
			}
			PreparedStatement pstmt = conexion.prepareStatement(
					"SELECT * FROM titular JOIN licencia ON licencia.DNI_titular=titular.CIF_NIF_NIE WHERE titular.REGISTRO= "
							+ dato);
			ResultSet rset = pstmt.executeQuery();

			// ResultSetMetaData rsmd = rset.getMetaData();
			int z = 0;
			while (rset.next()) {
				sRAZON_SOCIAL = rset.getString("RAZON_SOCIAL");
				sCIF_NIF_NIE = rset.getString("CIF_NIF_NIE");
				sTIPO_SOLICITANTE = rset.getString("TIPO_SOLICITANTE");
				iTELEFONO_MOVIL = rset.getInt("TELEFONO_MOVIL");
				iTELEFONO_FIJO = rset.getInt("TELEFONO_FIJO");
				sDIRECCION = rset.getString("DIRECCION");
				sMUNICIPIO = rset.getString("MUNICIPIO");
				iCOD_POSTAL = rset.getInt("COD_POSTAL");
				sEMAIL = rset.getString("EMAIL");
				z += 1;
			}

		} catch (SQLException s) {
			s.printStackTrace();
		}

		vista.devolverDatosExtra2();
	}


	public String getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(String solicitud) {
		this.solicitud = solicitud;
	}

	public String getDescripcion() {
		return sDescripcionActividad;
	}

	public void setDescripcion(String descripcion) {
		this.sDescripcionActividad = descripcion;
	}

	public String getActividad() {
		return ACTIVIDAD;
	}

	public void setActividad(String actividad) {
		this.ACTIVIDAD = actividad;
	}

	public String getCatastro() {
		return sREF_CATASTRAL;
	}

	public void setCatastro(String catastro) {
		this.sREF_CATASTRAL = catastro;
	}

	public Date getFechaInicio() {
		return dFechaInicioActividad;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.dFechaInicioActividad = fechaInicio;
	}

	public String getFotocopiaDNI() {
		return sFOTOCOPIA_DNI_CIF_NIE;
	}

	public void setFotocopiaDNI(String fotocopia) {
		this.sFOTOCOPIA_DNI_CIF_NIE = fotocopia;
	}

	public String getFotocopiaImpuestos() {
		return sFOTOCOPIA_ACTIVIDAD;
	}

	public void setFotocopiaImpuestos(String fotocopiaImpuestos) {
		this.sFOTOCOPIA_ACTIVIDAD = fotocopiaImpuestos;
	}

	public String getFotografia() {
		return sFOTOGRAFIAS;
	}

	public void setFotografia(String fotografia) {
		this.sFOTOGRAFIAS = fotografia;
	}

	public String getFotocopiaObra() {
		return sFOTOCOPIA_OBRA;
	}

	public void setFotocopiaObra(String fotocopiaObra) {
		this.sFOTOCOPIA_OBRA = fotocopiaObra;
	}

	public String getFotocopiaEscritura() {
		return sFOTOCOPIA_ESCRITURAS;
	}

	public void setFotocopiaEscritura(String fotocopiaEscritura) {
		this.sFOTOCOPIA_ESCRITURAS = fotocopiaEscritura;
	}

	public String getFotocopiaMemoria() {
		return sMEMORIA_ACTIVIDAD;
	}

	public void setFotocopiaMemoria(String fotocopiaMemoria) {
		this.sMEMORIA_ACTIVIDAD = fotocopiaMemoria;
	}

	public String getFotocopiaPago() {
		return sJUSTIFICANTE_PAGO;
	}

	public void setFotocopiaPago(String fotocopiaPago) {
		this.sJUSTIFICANTE_PAGO = fotocopiaPago;
	}

	public String getFotocopiaOtras() {
		return sFOTOCOPIA_OTROS;
	}

	public void setFotocopiaOtras(String fotocopiaOtras) {
		this.sFOTOCOPIA_OTROS = fotocopiaOtras;
	}

	public String getCertificado1() {
		return sCERTIFICADO_TECNICO1;
	}

	public void setCertificado1(String certificado1) {
		this.sCERTIFICADO_TECNICO1 = certificado1;
	}

	public String getCertificado2() {
		return sCERTIFICADO_COLEGIO_OFICIAL2;
	}

	public void setCertificado2(String certificado2) {
		this.sCERTIFICADO_COLEGIO_OFICIAL2 = certificado2;
	}

	public String getPlanos() {
		return sPLANOS_LOCAL;
	}

	public void setPlanos(String planos) {
		this.sPLANOS_LOCAL = planos;
	}

	public String getRazonSocial() {
		return sRAZON_SOCIAL;
	}

	public void setRazonSocial(String razonSocial) {
		this.sPLANOS_LOCAL = razonSocial;
	}

	public String getCif() {
		return sCIF_NIF_NIE;
	}

	public void setCif(String cif) {
		this.sCIF_NIF_NIE = cif;
	}

	public String getFax() {
		return sFAX;
	}

	public void setFax(String fax) {
		this.sFAX = fax;
	}

	public String getTipoPersona() {
		return sTIPO_SOLICITANTE;
	}

	public void setTipoPersona(String tipoPersona) {
		this.sTIPO_SOLICITANTE = tipoPersona;
	}

	public int getTlfMovil() {
		return iTELEFONO_MOVIL;
	}

	public void setTlfMovil(int tlfMovil) {
		this.iTELEFONO_MOVIL = tlfMovil;
	}

	public int getTlfFijo() {
		return iTELEFONO_FIJO;
	}

	public void setTlfFijo(int tlfFijo) {
		this.iTELEFONO_FIJO = tlfFijo;
	}

	public String getDireccion() {
		return sDIRECCION;
	}

	public void setDireccion(String direccion) {
		this.sDIRECCION = direccion;
	}

	public String getMunicipio() {
		return sMUNICIPIO;
	}

	public void setMunicipio(String municipio) {
		this.sMUNICIPIO = municipio;
	}

	public int getCp() {
		return iCOD_POSTAL;
	}

	public void setCp(int cp) {
		this.iCOD_POSTAL = cp;
	}

	public String getEmail() {
		return sEMAIL;
	}

	public void setEmail(String email) {
		this.sEMAIL = email;
	}

}
