package controlador;

import java.sql.Date;

import javax.swing.JOptionPane;

import modelo.MImplementacion;
import modelo.Modelo;
import vista.VCambioDeTitularidad;
import vista.VFichero;
import vista.VInicioSolicitud;
import vista.VInicioTitular;
import vista.VPantallaPrincipal;
import vista.Vista;

public class CImplementacionControl implements Controlador {
	private MImplementacion modelo;
	private VPantallaPrincipal vista;
	private VFichero vista2;
	private VInicioTitular vistaRegistroPersona;
	private VInicioSolicitud inicioSolicitud;
	private VCambioDeTitularidad cambioTitularidad;

	public CImplementacionControl() {
		super();

	}

	public void InicioSolicitud() {
		VInicioSolicitud ven = new VInicioSolicitud();
		ven.setControlador(this);
		ven.setModelo(modelo);
		this.setInicioSolicitud(ven);
		ven.setVisible(true);

	}

	private void setInicioSolicitud(VInicioSolicitud ven) {
		// TODO Auto-generated method stub
		this.inicioSolicitud = ven;
	}

	public void cambiarPantallaCambioTitularidad() {
		VCambioDeTitularidad ven = new VCambioDeTitularidad();
		ven.setVisible(true);

	}

	public void cambiarPantallaRegistroPersona() {
		System.out.println("-------------cambio de pantalla----------------");
		VInicioTitular ven = new VInicioTitular();
		ven.setControlador(this);
		this.setVistaRegistroPersona(ven);

		ven.setVisible(true);

	}

	private void setVistaRegistroPersona(VInicioTitular ven) {
		this.vistaRegistroPersona = ven;

	}

	public void cambioPantallaFichero() {
		VFichero ven = new VFichero();
		ven.setControlador(this);
		ven.setModelo(modelo);
		this.setVista2(ven);
		modelo.setVista2(ven);
		ven.setVisible(true);

	}

	private void setVista2(VFichero ven) {
		this.vista2 = ven;
	}

	public void cargarDatosBBDD() {
		String usuario = vista2.getTextFieldNombre();
		String pass = vista2.getTextFieldPass();
		String bbdd = vista2.getTextFieldURL();
		System.out.println(bbdd);
		modelo.setBBDD(usuario, pass, bbdd);

	}

	public void setModelo(Modelo modelo) {
		this.modelo = (MImplementacion) modelo;

	}

	public void setVista(Vista vista) {
		this.vista = (VPantallaPrincipal) vista;

	}

	public void obtenerDatosBBDD() {
		modelo.getBBDD();

	}

	public void enviarRegistro(String dato) {
		((MImplementacion) modelo).recogerDatosExtra(dato);

	}

	public void enviarRegistro2(String dato) {
		((MImplementacion) modelo).recogerDatosExtra2(dato);
	}

	//////////// INSERTAR BASE DE DATOS/////////////////////////////////.

	public int insertarATitular(String sCIF_NIF_NIE, String sNOMBRE, String sAPELLIDOS, String sMUNICIPIO,
			int iCOD_POSTAL, String sDIRECCION, int iTELEFONO_MOVIL, int iTELEFONO_FIJO, String sEMAIL, String sFAX,
			String sTIPO_SOLICITANTE, String sRAZON_SOCIAL) {

		return modelo.insertarATitular(sCIF_NIF_NIE, inicioSolicitud.getRegistro(), sNOMBRE, sAPELLIDOS, sMUNICIPIO,
				iCOD_POSTAL, sDIRECCION, iTELEFONO_MOVIL, iTELEFONO_FIJO, sEMAIL, sFAX, sTIPO_SOLICITANTE,
				inicioSolicitud.getCheckBoxDNI(), inicioSolicitud.getCheckFotografias(),
				inicioSolicitud.getCheckImpuestosActividad(), inicioSolicitud.getCheckEscrituras(),
				inicioSolicitud.getCheckJustificantePago(), inicioSolicitud.getCheckOtrasAutorizaciones(),
				inicioSolicitud.getCheckOtrasAutorizaciones());

	}

	public int insertarALicencia(String sREF_CATASTRAL, String sTIPO_SUELO, String sEMPLAZAMIENTO, String ACTIVIDAD,
			String dFechaSolicitud, String dFechaInicioActividad, String sEstadoLicencia, String sDescripcionActividad,String DNI_Titular) {
		
		return modelo.insertarALicencia(sREF_CATASTRAL, sTIPO_SUELO, sEMPLAZAMIENTO, ACTIVIDAD,
				inicioSolicitud.getCheckMemoriaActividad(), inicioSolicitud.getCheckPlanosLocal(), inicioSolicitud.getCheckFotocopiaObra(), inicioSolicitud.getCheckModeloTecnico(),
				inicioSolicitud.getCheckColegio(), DNI_Titular, dFechaSolicitud, dFechaInicioActividad, sEstadoLicencia,
				sDescripcionActividad);

	}
	
	public int insertarALicenciaTipo2(String DNI_Titular) {
		
		return insertarALicencia(inicioSolicitud.refCatastral.getText(), inicioSolicitud.tipoSuelo.getSelectedItem().toString(),
				inicioSolicitud.emplazamiento.getText(), inicioSolicitud.tipoActividad.getSelectedItem().toString(),
				inicioSolicitud.fechaSolicitud.getDate().toString(),
				inicioSolicitud.fechaInicioSolicitud.getDate().toString(),
				inicioSolicitud.estadoSolicitud.getSelectedItem().toString(), inicioSolicitud.descripcionActividad.getText(),DNI_Titular);

	}
}
